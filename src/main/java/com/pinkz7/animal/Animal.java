/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.animal;

/**
 *
 * @author ripgg
 */
public abstract class Animal {
    private String name;
    private int numberofLeg;
    
    public Animal(String name, int numberofLeg){
        this.name=name;
        this.numberofLeg=numberofLeg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberofLeg() {
        return numberofLeg;
    }

    public void setNumberofLeg(int numberofLeg) {
        this.numberofLeg = numberofLeg;
    }

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", numberofLeg=" + numberofLeg + '}';
    }
    
    public String getNickname() {
        return name;
    }
    
    public abstract void eat();
    public abstract void walk();       
    public abstract void speak();
    public abstract void sleep();
    
}
