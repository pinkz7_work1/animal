/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.animal;

/**
 *
 * @author ripgg
 */
public abstract class Poultry extends Animal{
    
    public Poultry(String name, int numberofLeg) {
        super(name, numberofLeg);
    }
    
    public abstract void fly();
    
}
